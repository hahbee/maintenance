-module(maintenance_home_controller, [Req]).
-author("Richard Diamond <wichard@hahbee.co>").

-export([under_construction/2]).

under_construction('GET', []) ->
    ok.
